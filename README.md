## About
Python3 utility, that imports apache access logs trough http connection and apache directory index plugin.
It can be run sequentially, and only imports the new access log data. It needs a mysql to import to. This is made for 
Grafana. It also have a geoip resolving feature, so the results can be plotted on a map.

### How it works:
Apache creates separate access log for each virtual host (or a huge one for all virtual host).
It exposes the access logs only to grafana trough http / https with autoindex.
The tool scrapes the logs, and only imports the new records / access log files.
It imports into a mysql DB, so other dataprocessing can be done there. On each session, it will process the new IPs, and
tries to resolve them to geoip locations. Extracts a list of main pages, that will make other aggregate queries faster.

### Requirements
- Python3
- Mysql
- Geoip
- Apache2 server with valid access logs setup

### Install instructions:
Setup you apache as reverse proxy:
- enable rewrite mod
- enable autoindex mod
- optional but strongly advised: enable ssl mod
   - advised: review / reconfig mod_ssl's conf (https://www.ssllabs.com/ssltest/) 
- optional: enable proxy_http mod
- optional: to get https dns host verified free certificates.
Letsencrypt and it's certbot (https://letsencrypt.org, https://certbot.eff.org)

Edit your apache2's main config, to customize the access log format
`/etc/apache2/apache2.conf:
LogFormat "%h %l %u %t \"%r\" %>s %b \"%{Referer}i\" \"%{User-agent}i\"" combined`

Put the log format string into the config.json: "access_log_format", so that the apacheLog can recognise it.
Register a hostname for your reverse proxy to access by grafana 
(can be registered only in apache as virtualhost, and in grafana in /etc/hosts file), and use
 the apache_log_access_virtualhost_setup.conf as template to setup.
 
In your existing / new virtualhosts use the given apache_reverse_proxy.conf template.

Optional: you can use the apache_http_redirect.conf to redirect all http traffic to https, but allow letsencrypt requests.

#### Database
Install mysql, and create 2 users, and a database.
- User with write privileges for importer
- User with read privileges for grafana to display, but not to write / delete
Use sql/db_create.sql to create the two tables needed.

#### Python3 dependencies
Install python3 dependencies, and make sure that GeoLite2-City.mmdb file location is configured in config.json.

#### Edit config.json
apache_url: where is your apache access logs are available (site root)  
access_log_base_filename: what is the base filename for the access log files. (Handles logrotate)  
access_log_format: in the reverse proxy, what is the setup apache log format (/etc/apache2/apache2.log)  
database: database access  parameters  
protected: object with site key, and a list of urls, that start with the protected content.
For example password recovery token:  
`"protected": {
    "security.corporation.com": [
      "/reset_password/token/"
    ]...
    }
` This will filter on the site: security.corporation.com/reset_password/token/*, 
and will not import these entries into the DB  
ip_filter: what IP's will not be logged. All internal IP range is added by default. Object, the key is the value, 
and if starts with it, it filters
`  "ip_filter": {
    "10." : "10.0.0.0/8", ...
` Will fitler: `10.0.0.1 - 10.255.255.255  
main_page_filter: What are not main pages. Regexp list (key is the value) to filter out. Useful for aggregating database 
of user visits.  
geoip: the location of the geoip database file.

#### Putting it in cronjob
On the grafana server, setup the cron job, by copying the `cron_job_apache_accesslog_import` to /etc/cron.d, and 
replacing the indicated variables:
- `$$USER$$`: linux user, to run the script, preferred unprivileged user
- `$$LOGPROCESSER_ABSOLUTE_PATH$$`: where the script is located without the `logprocesser` module directory 

After setup, wait and watch the /tmp for to appear: `/tmp/grafana_apache_import_error.lo`, and change last modification date. 
You can also run the script manually, for the first time. For the first time, it may take some time, 
to import all the content. If you have some data in the database, you can setup the grafana panels, see below.


#### import_sites.py output:

##### 1. stage
Lists all the sites found
##### 2. stage
Lists all the sites / access logs processed.
Every . at the end of the line represents MyDbUtils.transaction_batch amount of records.
##### 3. stage
Resolving URL, if they are `main page`.
##### 4. stage
Resolving unresolved IPs.
##### 5. stage
Statistics: how many access entry writen / site, and overall runtime.
If you are not interested in the output (which quite a lot), feel free to redirect to /dev/null. 

#### Grafana setup
Add new datasource, with mysql, and the read-only grafana user. 
You can use the examples in the examples directory (grafna_*.json).
For the example you will need the following plugins to work:

- Worldmap panel: https://grafana.com/grafana/plugins/grafana-worldmap-panel
- Heatmap panel: https://grafana.com/grafana/plugins/heatmap
- Pie chart panel: https://grafana.com/grafana/plugins/grafana-piechart-panel

All queries are using `site` variable, what is can setup as a dashboard variable (fixed, or user changable).
`site` is used in such way, that it can be `multi-value`. 

Dashboard settings -> Variables -> New
- Name: `site`
- Type: `query`
- Datasource: `grafana mysql read-only`
- Query: `select site from access_log group by site;`
- Sort: `Alphabetical(asc)`
- Multi-value: `True`

`Save dashboard`

##### grafana_traffic_by_sites_bytes_sum.json
![Pie chart Site per traffic in bytes](img/piechartSiteTrafficBytes.png)

Will show how much traffic a site has. It will create a pie chart, that show the ratio of your internet traffic 
between sites.
- Import json if possible.
- Create new Pie chart
- Query data source -> mysql grafana read-only datasource
- Copy query from json in raw sql  mode.
- Visualisation
  - Unit: `Bytes`
  - Show legend: `True`
  - Legend position: `Right side`
  - Legend values: `True` 
- Set the name of the panel

#### grafana_traffic_by_sites_bytes_timeline.json
![timeline traffic for sites](img/timelineTrafficSiteBytes.png)

Will display the traffic of your logged sites in a timeline.
- Import json if possible.
- Create new Pie chart
- Query data source -> mysql grafana read-only datasource
- Copy query from json in `Edit sql`  mode.
- Visualisation
  - Axes: Left Y:
      - Unit: `Bytes`
      - Scale: `linear`
  - Legend:
    - Show legend: `True`
    - As Table: `True`
    - To the right: `True`
    - Values / Tota: `True` 
- Set the name of the panel
- Optional: Create alerts


##### grafana_traffic_by_sites_request_sum.json
![Pie chart Site per traffic in requests](img/piechartTrafficSiteRequest.png)

Shows the sum of traffic by request ration by sites in Pie chart.
For setup, see part **grafana_traffic_by_sites_bytes_sum.json**

##### grafana_traffic_byt_sites_request_timeline.json
![timeline traffic for sites in requests](img/timelineTrafficRequests.png)

Shows the number of requests made ration by sites in Pie chart.
For setup, see part **grafana_traffic_by_sites_bytes_timeline.json**


##### grafana_traffic_world_map_bytes.json
![World map traffic in Megabytes](img/worldmapByBytes.png)

Will display the aggregated traffic on world map (by IP) in Megabytes. Fill the:
`ifnull(lat, 55.328612)` and `ifnull(lon, 3.924217)` with any location you like. (If geoip can't resolv IP's 
position, it puts into the North sea). May plot many circles to the same location.

Can be slow on large time-rage!

Setup manually:
- Import json, if possible.
- Create new world map panel
- Set datasource for mysql / grafana (if not done yet)
- Paste the Query from the json in `Edit sql` mode: "rawSql"
- Visual options ->
  - Map Visual Options
    - Unit: `MByte`, `MBytes`
  - Map Data Options
    - Location Data: `table`
    - Aggregation: `total`
    - scroll down!
    - Field Mapping
      - Table Query Format: `coordinates`
      - Location Name Field: `city`
      - Metric Field: `value`
      - Latitude Field: `lat`
      - Longitude Field: `lon`
- Name panel

##### grafana_traffic_world_map_requests.json
![World map traffic in requests](img/worldmapByRequests.png)

Will list aggregated traffic on world map (by IP)in requests made. Fill the:
`ifnull(lat, 55.328612)` and `ifnull(lon, 3.924217)` with any location you like. (If geoip can't resolv IP's 
position, it puts into the North sea). May plot many circles to the same location.

Can be slow on large time-rage!

Setup manually:
- Import json if possible.
- Create new world map panel
- Set datasource for mysql / grafana (if not done yet)
- Paste the Query from the json in `Edit sql` mode: "rawSql"
- Visual options ->
  - Map Visual Options
    - Unit: `MByte`, `MBytes`
  - Map Data Options
    - Location Data: `table`
    - Aggregation: `total`
    - scroll down!
    - Field Mapping
      - Table Query Format: `coordinates`
      - Location Name Field: `city`
      - Metric Field: `value`
      - Latitude Field: `lat`
      - Longitude Field: `lon`
- Name panel

##### grafana_page_visits_heatmap.json
![Heatmap / timeline of visited main pages](img/heatmapTimelineMainPageVisits.png)

Will show what main pages are visited, and when. `Page visit timemap (top 20)`. 
Will truncate the page name to 55 characters or less.

**Can be slow on large time-rage!**

- Create new heatmap
- Set datasource for mysql / grafana (if not done yet)
- Paste the query from the json to `Edit sql` mode: "rawSql" (Query A)
- Format as: `Time series`
- Visualisation:
  - Buckets / Space: `-3.5` (optional)
- Name panel
- Optional if you want a human readable daytime indicator, fill the Query B with `grafana_suntime.sql` see setup below.

##### grafana_page_visit_referer_heatmap.json
![Heatmap / timeline of most visited pages referers](img/heatmapTimelineMainPageReferers.png)

Will show where the main pages are visited from. (Other pages, google.com, m.facebook.com, ...) 
`Page referer visit timemap (top 20)`. Will truncate the page name to 55 characters or less.
- Import json if possible.
- Create new heatmap
- Set datasource for mysql / grafana (if not done yet)
- Paste the query from the json to `Edit sql` mode: "rawSql" (Query A)
- Format as: `Time series`
- Visualisation:
  - Buckets / Space: `-3.5` (optional)
- Name panel
- Optional if you want a human readable daytime indicator, fill the Query B with `grafana_suntime.sql` see setup below.

#### Grafana SunRiseSet setup
This is a mysql function that will give the current sunset / sunrise for the set coordinates.
You'll need function execution privileges under mysql to use this, in the given DB.

#### `grafana_suntime.sql`

![heatmap / timeline example for sun status](img/heatmapTimelineMainPageVisits.png)

This sql will create a data entry in heatmap timeline to indicate the sun's position. 
It will be displayed as `DAYLIGHT` most probably on top. To use it on an existing panel, add is as Query B.
It is needed because if you setup a datasource, you are stuck with it.

Can be slow on large time-rage!

To setup, change the following in the query:
- Location `47.586485, 19.042007`: change this to your location (Occurs 4 times)
- `...least(100,...`: the maximum minutes to display. Occurs 2 times, and can be lower / higher.
- `value` column: you can divide the value in the most outward select if you wish to.
- Changing resolution:
  - The current resolution is 300. (It will render 300 value withing the time range.)
  - `/299.0` change to your wanted resolution - 1
  - change the t0, t1, t2 to: count(t0)*count(t1)*count(t2) = wanted resolution.  
  If not enough or too much, you can add more or remove subsets.
  - `(t2.i + t1.i*10 + t0.i*100)` is assembled:
  - `( (0-9) + (0-9)*10 + (0-2)*100 )`
  - t2:
    - Lowest digit
    - count(t2) = 10 (0-9)
  - t1:
    - middle digit (tens))
    - t1.i*10 = t1.i * count(t2)
    - count(t1) = 10 (0-9) 
  - t0:
    - Highest digit, hundreds
    - t2.i*100 = t2.i * (count(t2) * count(t1))
    - count(t0) = 3 (0-2)
- For Heatmap - timeline must return the following columns: 
  - `time` - datetime
  - `metric` - string (varchar...)
  - `value` - numeric value
