drop table if exists `access_log`;
create table access_log (
site varchar(256),
INDEX(site) ,
ip varchar(32),
created timestamp,
INDEX(created),
method varchar(32),
uri blob,
base_uri blob,
uri_params blob,
response_code int,
response_size int,
referer blob,
user_agent blob);

drop table if exists `main_page`;
create table main_page(
site varchar(256),
INDEX(site),
main_page varchar(1024)
);

DROP TABLE IF EXISTS `geoips`;
CREATE TABLE `geoips` (
  `ip` varchar(24) DEFAULT NULL,
  `country` varchar(32) DEFAULT NULL,
  `city` varchar(64) DEFAULT NULL,
  `state` varchar(64) DEFAULT NULL,
  `lat` float DEFAULT NULL,
  `lon` float DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;