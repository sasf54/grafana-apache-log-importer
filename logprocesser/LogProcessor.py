#!/usr/bin/env python3
import json
import pprint
import datetime
import io, gzip, re

import requests
from apachelogs import LogParser
from bs4 import BeautifulSoup
from logprocesser import MyDbUtils
from logprocesser import GeoIpInserter


class LogProcessor:
    apache_log_parser: LogParser
    apache_url: str
    dbUtils: MyDbUtils
    assume_logrotate_amount = 20
    access_log_filename = "access_ssl.log"
    config: {}
    protected = {}
    ip_filters = []
    main_page_filter = {}
    # site / page
    # {}[]
    main_pages = {}

    def __init__(self, config_file_name: str) -> None:
        super().__init__()
        self.dbUtils = MyDbUtils.MyDbUtil()
        try:
            self.config = json.load(open(config_file_name, "rt"))
        except json.JSONDecodeError:
            print("Error in config.json!")
            exit(-1)
        if 'protected' in self.config:
            self.protected = self.config['protected']
        if 'ip_filter' in self.config:
            for ip_filter in self.config['ip_filter']:
                self.ip_filters.append(ip_filter)
        if 'main_page_filter' in self.config:
            for filter_ in self.config['main_page_filter']:
                self.main_page_filter[filter_] = re.compile("^" + filter_ + "$")
        if "apache_url" not in self.config:
            print("Variable apache_url is mandatory in config file!")
            exit(-1)
        if "access_log_base_filename" in self.config:
            self.access_log_filename = self.config['access_log_base_filename']
        self.apache_log_parser = LogParser(self.config['access_log_format'])
        self.apache_url = self.config['apache_url']

    def filter_entry(self, entry, site: str, last_entry):
        if last_entry is None or entry.request_time.astimezone(last_entry.tzinfo) <= last_entry:
            return
        url = entry.request_line.split(" ")[1]
        # if not set, use apache access log hostname for site variable
        server_name = None
        if 'server_name' in entry.__dict__:
            server_name = entry.server_name
        if server_name is None and 'virtual_host' in entry.__dict__:
            server_name = entry.virtual_host
        if (site is None or len(site) == 0) and 'hostname' in (server_name is not None and len(server_name) > 0):
            site = server_name
        if site in self.protected:
            for protected in self.protected[site]:
                if url.startswith(protected):
                    return
        for ip_filter in self.ip_filters:
            if entry.remote_host.startswith(ip_filter):
                return

        self.dbUtils.import_fast_continous(entry, site=site, last_entry=last_entry)

    def parse_logfile(self, url, site, last_entry=None):
        if url.startswith("http"):
            response = requests.get(url, timeout=30, stream=True)
            if url.endswith(".gz"):
                csv_gz_file = response.content
                f = io.BytesIO(csv_gz_file)
                with gzip.GzipFile(fileobj=f) as fh:
                    for line in fh:
                        entry = self.apache_log_parser.parse(line.decode("utf-8").rstrip("\n").lstrip("\x00").strip())
                        self.filter_entry(entry, site=site, last_entry=last_entry)
            else:
                for line in response.text.split("\n"):
                    if len(line) > 10:
                        entry = self.apache_log_parser.parse(line)
                        self.filter_entry(entry, site=site, last_entry=last_entry)
                        # pprint.pprint(entry.entry)
        else:
            with open(url, "r") as access_log:
                for line in access_log:
                    entry = self.apache_log_parser.parse(line)
                    self.filter_entry(entry, site=site, last_entry=last_entry)

    def import_site_full(self, site, last_entry=None):
        if last_entry is None:
            last_entry = self.dbUtils.last_entry_time(site)
        response = requests.get(self.apache_url + site)
        soup = BeautifulSoup(response.text, 'html.parser')
        access_logs = [None] * self.assume_logrotate_amount
        last_access = -1
        error_logs = [None] * self.assume_logrotate_amount
        last_error = 0
        for tr in soup.select("tr"):
            # is a vhost site
            if len(tr.select("td")) > 0 and tr.select("td img")[0].attrs['alt'] == "[   ]":
                log_filename = tr.select("td")[1].select("a")[0].getText()
                # print(log_filename)
                if log_filename.startswith(self.access_log_filename):
                    if log_filename == self.access_log_filename:
                        access_logs[0] = {}
                        access_logs[0]['filename'] = log_filename
                        access_logs[0]['modified'] = datetime.datetime.strptime(tr.select("td")[2].getText().strip(),
                                                                                "%Y-%m-%d %H:%M").astimezone(last_entry.tzinfo)
                        last_access = max(last_access, 0)
                    else:
                        short_filename = log_filename.replace(self.access_log_filename + ".", "")
                        short_filename = short_filename.replace(".gz", "")
                        access_logs[int(short_filename)] = {}
                        access_logs[int(short_filename)]['filename'] = log_filename
                        access_logs[int(short_filename)]['modified'] = datetime.datetime.strptime(
                            tr.select("td")[2].getText().strip(), "%Y-%m-%d %H:%M").astimezone(last_entry.tzinfo)
                        last_access = max(last_access, int(short_filename))

                #  #### Not implemented yet
                # if log_filename.startswith("error_ssl.log"):
                #     if log_filename == "error_ssl.log":
                #         error_logs[0] = {}
                #         error_logs[0]['filename'] = log_filename
                #         error_logs[0]['modified'] = datetime.datetime.strptime(tr.select("td")[2].getText().strip(),
                #         #   "%Y-%m-%d %H:%M").astimezone(last_entry.tzinfo)
                #     else:
                #         short_filename = log_filename.replace("error_ssl.log.", "")
                #         short_filename = short_filename.replace(".gz", "")
                #         error_logs[int(short_filename)] = {}
                #         error_logs[int(short_filename)]['filename'] = log_filename
                #         error_logs[int(short_filename)]['modified'] = datetime.datetime.strptime(
                #             tr.select("td")[2].getText().strip(), "%Y-%m-%d %H:%M").astimezone(last_entry.tzinfo)
                #         last_error = max(last_error, int(short_filename))
                #
        # processing access logs in date order (oldest first)
        if last_access != -1 :
            for index in range(last_access, -1, -1):
                if index == 0:
                    print("\nprocessing: "+ site + " / "+self.access_log_filename, end="")
                    self.parse_logfile(self.apache_url + site + "/" + self.access_log_filename, site,
                                       last_entry=last_entry)
                else:
                    # only process it, if newer than last entry
                    # to overwrite this behavior, pass last_entry = datetime.datetime.fromtimestamp(10.0)
                    if access_logs[index]['modified'] > last_entry:
                        print("\nprocessing: " + site + " / " + access_logs[index]['filename'], end="")
                        self.parse_logfile(self.apache_url + site + "/" + access_logs[index]['filename'], site,
                                           last_entry=last_entry)
        # force flushing in the current DB session!
        self.dbUtils.import_fast_continous(None, site, None)
        # print()

    def import_all_sites_full(self):
        response = requests.get(self.apache_url)
        if response.status_code != 200:
            print("http error:")
            print(str(response.status_code))
            return
        soup = BeautifulSoup(response.text, 'html.parser')
        sites = []
        for tr in soup.select("tr"):
            # is a vhost site
            if len(tr.select("td")) > 0 and tr.select("td img")[0].attrs['alt'] == "[DIR]":
                print(tr.select("td")[1].select("a")[0].attrs['href'])
                sites.append(tr.select("td")[1].select("a")[0].attrs['href'].rstrip("/"))
        response.close()
        for site in sites:
            self.import_site_full(site)
        print()

    def finish_the_import(self):
        self.dbUtils.save_main_pages()
        geoip_inserted = GeoIpInserter.GeoIpInserter(self.config)
        geoip_inserted.resolv_remaining_ips()


