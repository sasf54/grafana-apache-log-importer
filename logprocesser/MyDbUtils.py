#!/usr/bin/env python3
import datetime
import pprint
import sys
import json

import mysql.connector
from apachelogs import LogEntry
from apachelogs import LogParser
import re


class MyDbUtil:
    base_log_path = "../logs/"
    connection: mysql.connector.MySQLConnection
    insert_cursor: mysql.connector.connection.MySQLCursor
    insert_entry_sql = "INSERT INTO access_log (site, ip, created, method, uri, base_uri, uri_params, response_code, response_size, referer, user_agent) " \
                       "VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)"
    insert_geoip_sql = "INSERT INTO geoips (city, country, ip, lat, lon, state) " \
                       "VALUES (%s, %s, %s, %s, %s, %s)"
    dedupe_sql = "select count(*) from access_log where site = %s and created = %s and ip = %s and uri = %s and user_agent = %s"

    main_page_select_sql = "select count(*) from main_page where site=%s and main_page = %s"
    main_page_insert_sql = "insert into main_page (site,main_page) values (%s,%s)"
    apache_log_parser: LogParser
    inserted = {}
    transaction_counter = 0
    transaction_batch = 10000
    protected = {}
    main_page_filter = {}
    # site / page
    # {}[]
    main_pages = {}

    def __init__(self) -> None:
        super().__init__()
        try:
            config = json.load(open("logprocesser/config.json", "rt"))
        except json.JSONDecodeError:
            print("Error in config.json!")
            exit(-1)
        if 'protected' in config:
            self.protected = config['protected']
        if 'main_page_filter' in config:
            for filter_ in config['main_page_filter']:
                self.main_page_filter[filter_] = re.compile("^" + filter_ + "$")
        try:
            self.connection = mysql.connector.connect(
                **config['database']
            )
            self.insert_cursor = self.connection.cursor()
        except mysql.connector.errors.InterfaceError as exception:
            print("Problem with the mysql connection")
            print(exception)
            exit(-1)
        self.apache_log_parser = LogParser("%h %l %u %t \"%r\" %>s %O \"%{Referer}i\" \"%{User-Agent}i\"")

    def get_all_result(self):
        mycursor = self.connection.cursor()
        mycursor.execute("SELECT count(*) FROM access_log ")

        # myresult = mycursor.fetchone()
        myresult = mycursor.fetchall()
        for x in myresult:
            pprint.pprint(x)

    def insert_row(self, entry: LogEntry, site: str):
        url = entry.request_line.split(" ")[1]
        base_url = url.split("?")[0]
        parameters = "?".join(entry.request_line.split(" ")[1].split("?")[1:])
        if "&" in base_url:
            base_url = url.split("&")[0]
            parameters = "&".join(entry.request_line.split(" ")[1].split("&")[1:])
        if ";" in base_url:
            base_url = base_url.split(";")[0]
            parameters = ";".join(base_url.split(";")[1:]) + str(parameters)

        self.extract_main_page(base_url, entry.final_status, site)

        val = (site,  # site
               entry.remote_host,  # ip
               entry.request_time.strftime('%Y-%m-%d %H:%M:%S'),  # created
               entry.request_line.split(" ")[0],  # method
               url,  # uri
               base_url,  # base_uri
               parameters,  # uri_params
               entry.final_status,  # response_code
               entry.bytes_out,  # response_size
               entry.headers_in['Referer'],  # referer
               entry.headers_in['User-Agent']  # user_agent
               )
        self.insert_cursor.execute(self.insert_entry_sql, val)
        self.transaction_counter += 1
        if self.transaction_counter % self.transaction_batch == 0:
            self.force_commit()
        pass
        self.inserted[site] += 1

    def force_commit(self, msg="."):
        self.connection.commit()
        print(msg, end="")
        sys.stdout.flush()

    def last_entry_time(self, site):
        sql = "select max(created) from access_log WHERE site = %s"
        val = (site,)
        mycursor = self.connection.cursor()
        mycursor.execute(sql, val)
        result = mycursor.fetchone()[0]
        if result is None:
            result = datetime.datetime.fromtimestamp(10.0)
        return result.astimezone()

    def import_fast_continous(self, entry: LogEntry, site, last_entry):
        if entry is None:
            self.force_commit("*")
            return
        if site not in self.inserted:
            self.inserted[site] = 0
        if last_entry is not None and entry.request_time.astimezone(last_entry.tzinfo) > last_entry:
            self.insert_row(entry, site)

    def get_all_unlocated_ips(self):
        mycursor = self.connection.cursor()
        mycursor.execute(
            "select distinct(al.ip) from access_log as al where ip not in (select distinct ip from geoips)")

        # myresult = mycursor.fetchone()
        myresult = mycursor.fetchall()
        result = []
        for x in myresult:
            result.append(x[0])
        mycursor.close()
        return result

    def insert_geoip(self, city, country, ip, lat, lon, state):
        val = (city,
               country,
               ip,
               lat,
               lon,
               state)
        self.insert_cursor.execute(self.insert_geoip_sql, val)
        self.transaction_counter += 1
        # if self.transaction_counter % self.transaction_batch == 0:
        #     self.force_commit()

    def has_site_main_page(self, site, main_page):
        mycursor = self.connection.cursor()
        mycursor.execute(self.main_page_select_sql, (site, main_page))
        myresult = mycursor.fetchone()
        print(site + ": " + main_page + "\t: " + str(myresult[0]))
        result = int(myresult[0]) != 0
        mycursor.close()
        return result

    def insert_site_main_page(self, site, main_page):
        self.insert_cursor.execute(self.main_page_insert_sql, (site, main_page))

    def save_main_pages(self):
        print("main pages")
        pprint.pprint(self.main_pages)
        for site in self.main_pages:
            print(site + ": " + str(len(self.main_pages[site])))
            for main_page in self.main_pages[site]:
                if not self.has_site_main_page(site, main_page):
                    self.insert_site_main_page(site, main_page)
        self.force_commit("")
        pass

    def extract_main_page(self, base_url, status: int, site: str):
        # http status OK / redirect / not changed
        if 200 <= status < 400:
            main_page = True
            for filter_ in self.main_page_filter:
                if self.main_page_filter[filter_].match(base_url):
                    main_page = False
                    # print(site + ":\t  " + filter_ + "\t\t" + base_url)
            if main_page:
                if site not in self.main_pages:
                    self.main_pages[site] = []
                if base_url not in self.main_pages[site]:
                    self.main_pages[site].append(base_url)

    def extract_main_pages_all_db(self):
        print("processing:")
        mycursor = self.connection.cursor()
        mycursor.execute(
            "select access_log.site, base_uri, min(response_code)"
            "from access_log "
            "left join main_page on "
            "access_log.site = main_page.site and access_log.base_uri = main_page "
            "where main_page.main_page is null group by access_log.site, base_uri")
        numrows = 0
        for row in mycursor:
            numrows += 1
            if numrows % 1000 == 0:
                print(".")
            site = row[0]
            base_uri = row[1].decode("UTF-8")
            response_code = int(row[2])
            self.extract_main_page(base_uri, response_code, site)
        print(str(numrows))
        for site in self.main_pages:
            print('{:20}:\t{:8d}'.format(site, len(self.main_pages[site])))
            pprint.pprint(self.main_pages[site], indent=4)
        self.save_main_pages()
        print()
