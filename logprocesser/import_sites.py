#!/usr/bin/env python3

import os,sys,inspect
currentdir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parentdir = os.path.dirname(currentdir)
sys.path.insert(0,parentdir)

from apachelogs import LogParser
import pprint
from datetime import datetime
from logprocesser import MyDbUtils
from logprocesser import LogProcessor
from logprocesser import GeoIpInserter

dbUtils = MyDbUtils.MyDbUtil()
logProcessor = LogProcessor.LogProcessor("logprocesser/config.json")
start = datetime.now()
###########################
#  Here comes the process #
###########################

logProcessor.import_all_sites_full()
logProcessor.finish_the_import()

#######################
#  End of the process #
#######################

runtime = datetime.now() - start
print()
for site in dbUtils.inserted.keys():
    print('Inserted into [' + site + "] : " + str(dbUtils.inserted[site]) + ' records')

print("runtime: " + str(int(runtime.seconds / 60)) + ":" + str(runtime.seconds % 60) + "." + str(runtime.microseconds))
exit(0)
