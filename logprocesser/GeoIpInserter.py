import geoip2.database, geoip2.errors
from logprocesser import MyDbUtils
import pprint
import unicodedata
import sys, os.path
import html


class GeoIpInserter():
    reader: geoip2.database.Reader
    dbUtils: MyDbUtils
    processed = 0
    not_found = []

    def __init__(self, config: {}) -> None:
        super().__init__()
        if "geoip" in config:
            if not os.path.exists(config['geoip']):
                print("Config file does not exists: " + config['geoip'])
                exit(-1)
            self.reader = geoip2.database.Reader(config['geoip'])
        else:
            self.reader = geoip2.database.Reader('GeoLite2-City.mmdb')
        self.dbUtils = MyDbUtils.MyDbUtil()

    def resolv_remaining_ips(self):
        ip_list = self.dbUtils.get_all_unlocated_ips()
        table = {k: '&{};'.format(v) for k, v in html.entities.codepoint2name.items()}
        for ip in ip_list:
            self.processed += 1
            try:
                response = self.reader.city(ip)
                subdivision = ''
                if len(response.subdivisions) > 0:
                    subdivision = response.subdivisions[0].name.encode('ascii', 'xmlcharrefreplace').decode('UTF-8')
                    # subdivision = unicodedata.normalize('NFD', html.escape(response.subdivisions[0].name)).encode('ascii', 'ignore')
                city = response.city.name
                if city is not None:
                    city = city.encode('ascii', 'xmlcharrefreplace').decode('UTF-8')

                country = response.country.name
                if country is not None:
                    country = country.encode('ascii', 'xmlcharrefreplace').decode('UTF-8')
                self.dbUtils.insert_geoip(city,
                                          country,
                                          ip,
                                          response.location.latitude,
                                          response.location.longitude,
                                          subdivision)
            except geoip2.errors.AddressNotFoundError:
                self.not_found.append(ip)
            except IndexError:
                print("index error:")
                print(ip)
                pprint.pprint(response, indent=2)
                pprint.pprint(response.subdivisions, indent=2)
                exit(-1)
            except:
                print("general exception:")
                pprint.pprint(sys.exc_info())
                pprint.pprint(response, indent=2)
                pprint.pprint((city,
                               country,
                               ip,
                               response.location.latitude,
                               response.location.longitude,
                               subdivision))

                exit(-1)
        self.dbUtils.force_commit(None)
        print()
        print(str(len(self.not_found)) + " IPs were not found")
