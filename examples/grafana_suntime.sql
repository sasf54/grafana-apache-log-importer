select suntime.date as time,
greatest(0,least(100, timestampdiff(MINUTE,sunrise,  time(Date)))) - greatest(0,least(100, timestampdiff(MINUTE,sunset,  time(Date)))) as value,
'DAYLIGHT' as metric
from
(select dates.Date,
    SunRiseSet(dates.Date, 47.586485, 19.042007, 'official','sunrise') sunrise,
    SunRiseSet(dates.Date, 47.586485, 19.042007, 'official','sunset') sunset,
    timestampdiff(MINUTE, SunRiseSet(dates.Date, 47.586485, 19.042007, 'official','sunrise') ,SunRiseSet(dates.Date, 47.586485, 19.042007, 'official','sunset') )/2 day_length,
    dates.partial as partial
    from(
      select date_add($__timeFrom(), INTERVAL timestampdiff(SECOND,$__timeFrom(), $__timeTo())*((t2.i + t1.i*10 + t0.i*100)/299.0) SECOND) as Date,
    ((t2.i + t1.i*10 + t0.i*100)/299.0) as partial
      from
     (select 0 i union select 1 union select 2 ) t0,
     (select 0 i union select 1 union select 2 union select 3 union select 4 union select 5 union select 6 union select 7 union select 8 union select 9) t1,
     (select 0 i union select 1 union select 2 union select 3 union select 4 union select 5 union select 6 union select 7 union select 8 union select 9) t2
    order by Date
     ) dates) suntime